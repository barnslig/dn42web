#!/usr/bin/python2
import pymongo, subprocess

db = pymongo.Connection()
db = db.dn42.autonomous_systems
db.remove()

# own as
## your asn
ownASN = "64753"
handle = {
	"asn": ownASN,
	"peerings": [],
	"subnets": []
}
## peerings
peerings = subprocess.Popen(['birdc', 'show', 'protocols', 'all'], stdout=subprocess.PIPE).stdout.read().split("dn42_")
for p in peerings:
	p = p.split("\n")
	if p[0][0:4] == "BIRD":
		continue
	if "Established" in p[0]:
		for l in p:
			if "Neighbor AS:" in l:
				l = l.replace("    Neighbor AS:      ", "")
				if l not in handle["peerings"]:
					handle["peerings"].append(l)
					# append it to the peering handles
					phandle = {
						"asn": l,
						"peerings": [ownASN],
						"subnets": []
					}
					db.save(phandle)
## subnets
subnets = subprocess.Popen(["birdc", "show", "route"], stdout=subprocess.PIPE).stdout
for line in subnets.readlines():
	if line[0:4] == "BIRD":
		continue
	if line[0:1] != " ":
		if line not in handle["subnets"]:
			handle["subnets"].append(line.split(" ")[0])

db.save(handle)

def splitBirdTable(f):
	out = []
	for line in f.readlines():
		if line[0:4] == "BIRD":
			continue
		try:
			foo = int(line[0:1])
			out.append(line)
		except:
			out[-1] += line

	return out

rawTable = subprocess.Popen(["birdc", "show", "route", "all"], stdout=subprocess.PIPE).stdout

# process the subnets
for subnet in splitBirdTable(rawTable):
	netAndAS = subnet.split("Type: ")[0]
	net = netAndAS.split("via")[0].replace(" ", "")
	asn = netAndAS.split("[")[-1][2:-4]

	# get the as pathes
	as_pathes = []
	for x in subnet.split("BGP."):
		if "as_path" in x:
			as_pathes.append(x.replace("\n", "").replace("\t", "").replace("as_path: ", ""))	

	# get the peerings
	peerings = []
	for path in as_pathes:
		asns = path.split(" ")
		x = 0
		while x < len(asns) - 1:
			peerings.append((asns[x], asns[x + 1]))
			peerings.append((asns[x + 1], asns[x]))

			x += 1

	# put the things into the database
	for p in peerings:
		handle = db.find_one({"asn": p[0]})
		print handle
		if handle:
			# the peering
			if p[1] not in handle["peerings"]:
				handle["peerings"].append(p[1])
			# the subnet
			if p[0] == asn:
				if net not in handle["subnets"]:
					handle["subnets"].append(net)
			db.save(handle)
		else:
			handle = {
				"asn": p[0],
				"peerings": [
					p[1]
				],
				"subnets": []
			}
			if p[0] == asn:
				handle["subnets"].append(net)
			db.save(handle)
