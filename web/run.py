#!/usr/bin/env python2
import pymongo, netaddr, uuid, pydot, subprocess
from bottle import Bottle, run, request, static_file, template

db = pymongo.Connection()
db = db.dn42.autonomous_systems

app = Bottle()

@app.route("/")
def index():
	return template("templates/index.tpl", asns=db.find())

@app.route("/AS<asn:int>")
def showAS(asn):
	asn = str(asn)
	handle = db.find_one({"asn": asn})
	if handle:
		subnets = []
		# get the subnet whois
		for s in handle["subnets"]:
			if s:
				th = {
					"cidr": s,
					"descr": "",
					"country": "",
					"netname": ""
				}
				try:
					whois = subprocess.Popen(["whois", "-h", "172.22.177.77", s.split("/")[0]], stdout=subprocess.PIPE).stdout.read().split("/usr/etc/dn42/branch/data/inetnum/")[-1].split("\n")
					for x in whois:
						if x[0:1] == "%":
							continue
						if x[0:5] == "descr":
							th["descr"] = x.replace("descr:              ", "")
						if x[0:7] == "country":
							th["country"] = x.replace("country:            ", "")
						if x[0:7] == "netname":
							th["netname"] = x.replace("netname:            ", "")
					if th["netname"] == "DN42-IP4NET":
						th = {
							"cidr": s,
							"descr": "",
							"country": "",
							"netname": ""
						}
				except:
					pass
				subnets.append(th)
		
		# get the as whois
		asdescr = ""
		try:
			whois = subprocess.Popen(["whois", "-h", "172.22.177.77", "AS{0}".format(asn)], stdout=subprocess.PIPE).stdout.read().split("/usr/etc/dn42/branch/data/aut-num/")[1].split("\n")
			for x in whois:
				if x[0:5] == "descr":
					asdescr = x.replace("descr:              ", "")
		except:
			pass

		return template("templates/as.tpl", handle=handle, subnets=subnets, asdescr=asdescr)

@app.route("/path/<asn>/<maxsub:int>")
def showPath(asn, maxsub):
	subprocess.call("rm /tmp/dn42_*.png", shell=True)

	handle = db.find_one({"asn": asn})
	if handle:
		graph = pydot.Dot(graph_type='digraph')
		for p in handle["peerings"]:
			graph.add_edge(pydot.Edge(handle["asn"], p))
			subpeers = db.find_one({"asn": p})["peerings"]
			if len(subpeers) < maxsub:
				for sp in subpeers:
					if sp == handle["asn"]:
						continue
					graph.add_edge(pydot.Edge(p, sp))
	filename = "dn42_{0}.png".format(uuid.uuid1())
	graph.write_png("/tmp/" + filename)

	return static_file(filename, root="/tmp/", mimetype="image/png")

@app.route("/search")
def search():
	q = request.query.q
	results = []
	net = q.split("/")

	# ip address
	if netaddr.valid_ipv4(q) and "." in q:
		# check all subnets for this ip address
		for handle in db.find():
			for net in handle["subnets"]:
				if netaddr.IPAddress(q) in netaddr.IPNetwork(net):
					results.append('{0} in {1} (<a href="/AS{2}">AS{2}</a>)'.format(
						q,
						net,
						handle["asn"]
					))

	# subnet
	elif len(net) == 2 and netaddr.valid_ipv4(net[0]) and net[1].isdigit():
		# check all subnets
		for handle in db.find():
			for net in handle["subnets"]:
				if net == q:
					results.append('{0} announced by <a href="/AS{1}">AS{1}</a>'.format(
						net,
						handle["asn"]
					))

	# asn
	elif len(q) is 5 and q.isdigit():
		handle = db.find_one({"asn": q})
		if handle:
			results.append('<a href="/AS{0}">AS{0}</a>'.format(handle["asn"]))
	elif q[0:2] == "AS" and len(q[2:len(q)]) == 5 and q[2:len(q)].isdigit():
		handle = db.find_one({"asn": q[2:len(q)]})
		if handle:
			results.append('<a href="/AS{0}">AS{0}</a>'.format(handle["asn"]))

	# check for results
	if len(results) == 0:
		results = False

	return template("templates/search.tpl", results=results, q=q)

run(app, host='0.0.0.0', port=8080, server='tornado')
