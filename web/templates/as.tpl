<h1>AS{{ handle["asn"] }} <small>{{ asdescr }}</small></h1>

<section id="stats-section">
	<h2>Stats:</h2>
	<dl id="stats">
		<dt>Peerings</dt>
		<dd>{{ len(handle["peerings"]) }}</dd>

		<dt>Subnets</dt>
		<dd>{{ len(handle["subnets"]) }}</dd>
	</dl>
</section>

<section id="peerings-section">
	<h2>Peerings:</h2>
	<ul id="peerings">
		%for p in handle["peerings"]:
			<li><a href="/AS{{ p }}">{{ p }}</a></li>
		%end
	</ul>
</section>

<section id="subnets-section">
	<h2>Subnets:</h2>
	%if len(subnets) != 0:
		<table id="subnets" border="1">
			<tr>
				<th>Subnet</th>
				<th>Netname</th>
				<th>Description</th>
				<th>Country</th>
			</tr>
			%for s in subnets:
				<tr>
					<th>{{ s["cidr"] }}</th>
					<td>{{ s["netname"] }}</td>
					<td>{{ s["descr"] }}</td>
					<th><img src="http://bgp.he.net/images/flags/{{ s["country"].lower() }}.gif" alt="{{ s["country"] }}" title="{{ s["country"] }}" /></th>
				</tr>
			%end
		</table>
	%else:
		<h3>This serious router isn't announcing any subnets.</h3>
	%end
</section>

<section id="peering-path-image">
	<h2>Peering path:</h2>
	<p><img src="/path/{{ handle["asn"] }}/17" alt="" /></p>
</section>

%rebase templates/layout title='AS{0}'.format(handle["asn"])