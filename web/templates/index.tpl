<h3>All announced AS-Numbers</h3>
<ul id="asns">
	%for asn in asns:
		<li><a href="/AS{{ asn["asn"] }}">{{ asn["asn"] }}</a></li>
	%end
</ul>

%rebase templates/layout title='Index'