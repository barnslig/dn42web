<!doctype html>
<html>
	<head>
		<title>The DN42 — {{ title }}</title>
		<meta charset="utf-8" />
		<style type="text/css">
			.clear {
				clear: both;
			}

			header {
				border-bottom: 1px #000 dotted;
			}
			header > nav > ul {
				margin: 0;
				padding: 0;
			}
			header > nav > ul > li {
				float: left;
				list-style: none;
				padding-right: 7px;
				border-right: 1px #000 solid;
				margin-left: 7px;
			}
			header > nav > ul > li:last-child {
				border-right: 0;
			}
		</style>
	</head>
	<body>
		<header>
			<nav>
				<ul>
					<li><a href="/">Index</a></li>
				</ul>
			</nav>
			<form action="/search" method="get">
				<input type="search" name="q" placeholder="Search by IP address, ASN, Subnet, …" style="width:350px;" />
				<input type="submit" value="Let's go!" />
			</form>
		</header>
		<article>
			%include
		</article>
	</body>
</html>