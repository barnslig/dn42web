<h1>Search results for {{ q }}</h1>

<section id="search-results-section">
	% if results == False:
		<h2>No results :(</h2>
	%else:
		<ul id="search-results">
			%for r in results:
				<li>{{!r}}</a></li>
			%end
		</ul>
	%end
</section>

%rebase templates/layout title='Search results for {0}'.format(q)